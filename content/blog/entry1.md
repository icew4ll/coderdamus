---
title: Neovim IDE
tags: ['neovim', 'lua', 'ide', 'workflow']
author: ['coderdamus']
created: 2020-10-24
category: Digital
image: ./images/danil_silantev_F6Da4r2x5to.jpg
excerpt: "Markdown is intended to be as easy-to-read and easy-to-write as is feasible. Readability, however, is emphasized above all else. A Markdown-formatted document should be publishable as-is, as plain text, without looking like it's been marked up with tags or formatting instructions."
---

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)

# Heading 1 {style="color:red;"}

## Heading 2 {style="color:yellow;"}

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Normal Text

Lorem markdownum artesque tu quidem lanigeris! Amari aliquis Ismarios,
hospitiique nullum ab enim Pagasaea probabant armis iniuria inponi. Primus
Aonius graves at inductas nec motu, qui pinetis. Anxius nec ibimus utque illa
circa video est fuit labores alas. Huic per quantum undis, Themis et quamvis
gramine missisque leonibus.

## Blockquotes

> Meo locum plurimus laudatos exstantibus fistula nocte Ancaeo denique montanum.
> Dissipat nullique tenax; aut una lacessit purpureus sumptis inlaesos,
> Polypemonis quisque blanditus. Obscenas rumpitque numerum effluxere,
> pronusque: Mygdonidesque precantia erat potes undis. Resurgere conplet velut
> freta miram enim, maiorque nec nec inaniter mensura et ipse artus flebam
> gentisque solus.

## Ordered List

1. Quotiens urbis Charaxi referre
2. Terris acti iussit extrema
3. Vel totis Iove locum forma
4. Esse neve illi crimen ripis et crimina

## Unordered List

- Quotiens urbis Charaxi referre
  - freta miram enim
  - freta miram enim
    - maiorque nec nec
    - maiorque nec nec
    - maiorque nec nec
- Terris acti iussit extrema
- Vel totis Iove locum forma
- Esse neve illi crimen ripis et crimina

## Codeblock

```bash
egrep \
'wp-login|xmlrpc.php|upload-handler.php|phpmyadmin/sql.php' \
/www/$dom/*/public_html/logs/access_log \
| grep $(/bin/date '+%Y:%H:%M') \
| awk -F' ' '{print $1}' \
| awk -F':' '{print $2}' \
| grep -v 'ip' \
| sort \
| uniq -c \
| sort -nk1
```

## Table

| Tables   |      Are      |   Cool |
| -------- | :-----------: | -----: |
| col 1 is | left-aligned  | \$1600 |
| col 2 is |   centered    |   \$12 |
| col 3 is | right-aligned |    \$1 |

## Table

| Tables   |      Are      |  Cool  |
| -------- | :-----------: | :----: |
| col 1 is | left-aligned  | \$1600 |
| col 2 is |   centered    |  \$12  |
| col 3 is | right-aligned |  \$1   |

## Images

![Photo by Danil Silantev on Unsplash](./images/danil_silantev_F6Da4r2x5to.jpg)
