import '~/assets/scss/main.scss'
import DefaultLayout from "~/layouts/Default.vue";
import ClickOutside from "v-click-outside";
import { VTooltip, VPopover, VClosePopover } from "v-tooltip";
import InfiniteLoading from "vue-infinite-loading";

export default function(Vue, { router, head, isClient }) {
  Vue.component("Layout", DefaultLayout);
  Vue.use(ClickOutside);

  if (isClient) {
    VTooltip.options.defaultPlacement = "top-end";
    VTooltip.options.defaultClass =
      "bg-black text-xs px-2 leading-normal py-1 rounded absolute text-gray-400 max-w-xs ml-2 mt-3";
    VTooltip.options.defaultBoundariesElement = document.body;

    Vue.directive("tooltip", VTooltip);
    Vue.directive("close-popover", VClosePopover);
    Vue.component("v-popover", VPopover);
    Vue.use(InfiniteLoading);
  }
}
